var tool_type = "brush";
function canvas_function() {
  const canvas = document.getElementById('canvas');
  const ctx = canvas.getContext('2d');

  function adjustCanvas(){  
    canvas.width  = window.innerWidth * 0.62;
    canvas.height = window.innerHeight * 0.75;
  }  
  adjustCanvas();

  var painting = false;
  var hasTextinput = false;
  var startX = 0;
  var startY = 0;
  
  var brush_size = 10;
  var brush_color = "#000000";
  var text_font = "Serif";
  var text_size = 12;

  var historyArray = new Array();
  var h_step = -1;
  
  function renew_brush_info() {
    brush_size = document.getElementById("brush_size").value;
    brush_color = document.getElementById("brush_color").value;
    ctx.strokeStyle = brush_color;
    ctx.lineWidth = brush_size;
    ctx.lineCap = "round";
  }
  function renew_text_info() {
    text_font = document.getElementById("text_font").value;
    text_size = document.getElementById("text_size").value;
    ctx.font = `${text_size}px ${text_font}`;
  }

  canvas.addEventListener("mousedown", start_draw);
  canvas.addEventListener("mouseup", stop_draw);
  function start_draw(mouse) {
    painting = true;
    startX = mouse.clientX;
    startY = mouse.clientY;
    ctx.beginPath();
    ctx.moveTo(mouse.clientX, mouse.clientY);
    if(tool_type=="text") type_text(mouse);
    if(tool_type=="circle" || tool_type=="triangle" || tool_type=="rectangle" || tool_type=="line") hPush();
  }
  function stop_draw() {
    painting = false;
    if(tool_type=="circle" || tool_type=="triangle" || tool_type=="rectangle" || tool_type=="line") redo();
    else hPush();
  }

  canvas.addEventListener("mousemove", draw);
  function draw(mouse) {
    if(!painting) return;
    console.log(tool_type);
    switch(tool_type) {
      case 'brush':
        draw_line(mouse);
        break;
      case 'eraser':
        erase(mouse);
        break;
      case 'text':
        break;
      case 'line':
        draw_straight(mouse);
        break;
      case 'circle':
        undo();
        renew_brush_info();
        ctx.beginPath();
        let radius =((mouse.clientX-startX)**2+(mouse.clientY-startY)**2)**(1/2);
        ctx.arc(startX, startY, radius, 0, Math.PI*2, true);
        ctx.stroke();
        hPush();
        break;
      case 'triangle':
        undo();
        renew_brush_info();
        ctx.beginPath();
        ctx.moveTo(startX, startY);
        ctx.lineTo(mouse.clientX,mouse.clientY);
        ctx.lineTo(startX-(mouse.clientX-startX), mouse.clientY);
        ctx.closePath();
        ctx.stroke();
        hPush();
        break;
      case 'rectangle':
        undo();
        renew_brush_info();
        ctx.strokeRect(startX, startY, mouse.clientX-startX, mouse.clientY-startY);
        hPush();
        break;
      default:

    }
  }
  function draw_line(mouse) {
    if(!painting) return;
    renew_brush_info();
    ctx.lineTo(mouse.clientX, mouse.clientY);
    ctx.stroke();
  }
  function erase(mouse) {
    renew_brush_info();
    ctx.clearRect(mouse.clientX-brush_size/2, mouse.clientY-brush_size/2, brush_size, brush_size);
    //ctx.fillRect(mouse.clientX, mouse.clientY, brush_size, brush_size);
  }
  function draw_straight(mouse) {
    renew_brush_info();
    ctx.lineTo(mouse.clientX, mouse.clientY);
    ctx.stroke();
  }
  function type_text(mouse) {
    if(hasTextinput) return;
    else hasTextinput = true;
    var input = document.createElement('input');

    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = (mouse.clientX - 4) + 'px';
    input.style.top = (mouse.clientY - 4) + 'px';

    input.onkeydown = (e) => {
      if(e.key=="Enter"){
        ctx.textBaseline = 'top';
        ctx.textAlign = 'left';
        renew_text_info();
        ctx.fillText(input.value, mouse.clientX - 4, mouse.clientY - 4);
        input.parentNode.removeChild(input);
        hasTextinput = false;
        hPush();
        stop_draw();
      }
    };

    document.body.appendChild(input);

    input.focus();
  }  
  
  function hPush() { 
    h_step++;      
    if (h_step < historyArray.length) { historyArray.length = h_step; }
    historyArray.push(ctx.getImageData(1, 1, canvas.width, canvas.height));
  }
  document.getElementsByName("undo")[0].addEventListener("click", undo);
  function undo(){
    if(h_step > 0) {
      h_step--;
      ctx.putImageData(historyArray[h_step], 1, 1);
    }
    else if(h_step == 0){
      h_step = -1;
      historyArray = new Array();
      reset();
    }
  }
  document.getElementsByName("redo")[0].addEventListener("click", redo);
  function redo() {
    if(h_step < historyArray.length-1) {
      h_step++;
      ctx.putImageData(historyArray[h_step], 1, 1);
    }
  }
  document.getElementsByName("reset")[0].addEventListener("click", reset);
  function reset() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    h_step = -1;
    historyArray = new Array();
  } 

  document.getElementById('img_uploader').addEventListener('change', handleImage, false);
  function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            ctx.drawImage(img,0,0);
            hPush();
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);     
  }
  
  // var alltools = Array.from(document.getElementsByClassName("tool"));
  // alltools.forEach(
  //   ()=>{
  //     document.addEventListener("blur",(e)=>{
  //       console.log(e.name);
  //       if(e.name==tool_type) setTimeout(function() {e.focus();}, 10);
  //     }
  //     )
  //   });
  window.addEventListener('resize', adjustCanvas(), false);
}

function upload() {
  document.getElementById("img_uploader").click();
}

function switch_tool(name) {
  var canvas = document.getElementById('canvas');
  tool_type = name;
  switch(tool_type) {
    case 'brush':
      canvas.style.cursor = 'url(cur/brush.png), auto';
      
      console.log("here " + tool_type);
      break;
    case 'eraser':
      canvas.style.cursor = 'url(cur/eraser.png), auto';
      break;
    case 'text':
      canvas.style.cursor = 'url("cur/text.png"), auto';
      break;
    case 'line':
      canvas.style.cursor = 'url("cur/line.png"), auto';
      break;
    case 'circle':
      canvas.style.cursor = 'url("cur/circle.png"), auto';
      break;
    case 'triangle':
      canvas.style.cursor = 'url("cur/triangle.png"), auto';
      break;
    case 'rectangle':
      canvas.style.cursor = 'url("cur/rectangle.png"), auto';
      break;
    default:
      canvas.style.cursor = 'auto';
  }
  
}