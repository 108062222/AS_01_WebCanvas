# Software Studio 2021 Spring
## Assignment 01 Web Canvas 108062222


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    點擊旁邊的icon就可以使用，其他東西都可以直接用滑鼠調整。
    text這個功能我會滑鼠先在canvas上點擊後生成一個input box，
    在inputbox輸入你要的文字後按Enter就會畫到canvas上了。
    eraser的大小跟brush的大小一樣。
### Function description

    Nothing here

### Gitlab page link

    https://108062222.gitlab.io/AS_01_WebCanvas

### Others (Optional)

    So difficult XD~

<style>
table th{
    width: 100%;
}
</style>
